################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/ATP.cpp \
../src/ATPController.cpp \
../src/Hero.cpp \
../src/Krebs\ Cycle.cpp \
../src/Mitochondria.cpp \
../src/XForm.cpp 

OBJS += \
./src/ATP.o \
./src/ATPController.o \
./src/Hero.o \
./src/Krebs\ Cycle.o \
./src/Mitochondria.o \
./src/XForm.o 

CPP_DEPS += \
./src/ATP.d \
./src/ATPController.d \
./src/Hero.d \
./src/Krebs\ Cycle.d \
./src/Mitochondria.d \
./src/XForm.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -v -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Krebs\ Cycle.o: ../src/Krebs\ Cycle.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -v -MMD -MP -MF"src/Krebs Cycle.d" -MT"src/Krebs\ Cycle.d" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


